// $Id:$

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation


INTRODUCTION
------------
This module supports the corresponding theme, to correctly display it contents.
Some of the functionality used for the visual appearance, needed to involve 
diverse hooks, which can only be fired from the context of a module. So this 
module can be considered as part of the learn:line NRW theme.


INSTALLATION
------------
Just put the folder containing this document in one of Drupal's modules 
directories. The preferred one is 'sites/all/modules/custom'.
After placing the folder, you can go to the module administration and activate
it.
