<?php
// $id:$

/**
 * @file
 * Config class file.
 */

namespace learnline\conf\classes;

use ArrayAccess, Countable, IteratorAggregate;

class Config implements ArrayAccess, Countable, IteratorAggregate {
  /**
   * Setting: Keep settings persistent.
   * 
   * @var string
   */
  const SETTING__PERSIST = 'learnline_conf_admin_settings_persist';
  
  /**
   * Setting: Deactivate WYSIWYG's HTML filtering.
   * 
   * @var string
   */
  const SETTING__WYSIWYG_HTML_FILTER_DISABLE = 'learnline_conf_admin_settings_wysiwyg_html';
  
  /**
   * Setting: Introduce view mode 'overview_teaser'.
   * 
   * @var string
   */
  const SETTING__OVERVIEW_TEASER_ENABLE = 'learnline_conf_admin_settings_overview_teaser';
  
  /**
   * Setting: Enable auto-adding of image captions.
   * 
   * @var string
   */
  const SETTING__IMAGE_CAPTION_ENABLE = 'learnline_conf_admin_settings_image_caption';
  
  /**
   * Setting: Selector for images to add a caption.
   * 
   * @var string
   */
  const SETTING__IMAGE_CAPTION_SELECTOR = 'learnline_conf_admin_settings_image_caption_selector';
  
  /**
   * Setting: Enable auto-generation of image links using the page overlay.
   * 
   * @var string
   */
  const SETTING__IMAGE_LINKS_ENABLE = 'learnline_conf_admin_settings_image_link_enable';
  
  /**
   * Setting: Selector for images to be linked using page overlay.
   * 
   * @var string
   */
  const SETTING__IMAGE_LINKS_SELECTOR = 'learnline_conf_admin_settings_image_link_selector';
  
  /**
   * Setting: Fix width of server-side created <figcaption> tags.
   * 
   * @var string
   */
  const SETTING__FIX_FIGCAPTION_WIDTH = 'learnline_conf_admin_settings_fix_figcaption_width';
  
  /**
   * Setting: Allowed file extensions.
   * 
   * @var string
   */
  const SETTING__ALLOWED_FILE_EXTENSIONS = 'learnline_conf_admin_settings_allowed_file_extensions';
  
  /**
   * Setting: Whether to use Alertify JS or not.
   * 
   * @var string
   */
  const SETTING__ACTIVATE_ALERTIFY = 'learnline_conf_admin_settings_alertify';
  
  /**
   * An array, holding arrays of object instances for each inherited class.
   *
   * @var array
   */
  protected static $instance = array();
  
  /**
   * Settings array, containing the actual values.
   *
   * @var array
   */
  protected $settings = array();
  
  /**
   * 
   * 
   * @return array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__PERSIST => TRUE,
      self::SETTING__WYSIWYG_HTML_FILTER_DISABLE => TRUE,
      self::SETTING__OVERVIEW_TEASER_ENABLE => TRUE,
      self::SETTING__IMAGE_CAPTION_ENABLE => TRUE,
      self::SETTING__IMAGE_CAPTION_SELECTOR => 'img.show-caption',
      self::SETTING__IMAGE_LINKS_ENABLE => TRUE,
      self::SETTING__IMAGE_LINKS_SELECTOR => 'a[rel="overlay"],a[rel="lightbox"],a[rel="shadowbox"]',
      self::SETTING__FIX_FIGCAPTION_WIDTH => TRUE,
      self::SETTING__ALLOWED_FILE_EXTENSIONS => 'pdf,png,jpg,jpeg,gif',
      self::SETTING__ACTIVATE_ALERTIFY => FALSE,
    );
  } //function
  
  protected function initSettings() {
    foreach (self::getDefaultSettings() as $setting => $default) {
      $this->settings[$setting] = variable_get($setting, $default);
      if (!isset($this->settings[$setting])) {
        throw new \RuntimeException(
            t("Missing setting '@setting'.", array('@setting' => $setting)));
      } //if
    } //foreach
    
    return $this;
  } //function
  
  public function __construct() {
    $this->initSettings();
  } //function
  
  /**
   * Returns a new object of this class. Uses clone and the reset() method, to
   * avoid unnecessary re-initialization.
   * 
   * @return \learnline\conf\classes\Config
   */
  public static function getNewInstance() {
    $instanceClassName = get_called_class();
    if (!isset(self::$instance[$instanceClassName])) {
      self::$instance[$instanceClassName] = array();
      return self::$instance[$instanceClassName][] = new $instanceClassName();
    } //if
    
    $clone = clone reset(self::$instance[$instanceClassName]);
    return self::$instance[$instanceClassName][] = $clone->reset();
  } //function

  /**
   * Returns an array of all initialized objects of this class.
   * 
   * @return array
   */
  public static function getInstances() {
    $instanceClassName = get_called_class();
    if (!is_array(self::$instance[$instanceClassName])) {
      return array();
    } //if
    
    return self::$instance[$instanceClassName];
  } //function
  
  /**
   * Returns the last initialized object of this class.
   * If no object has been initialized yet, a new one will be returned.
   * 
   * @return \learnline\conf\classes\Config
   */
  public static function getLastInstance() {
    $instanceClassName = get_called_class();
    if (!isset(self::$instance[$instanceClassName]) || 
        !is_array(self::$instance[$instanceClassName])) {
      return self::getNewInstance();
    } //if
    
    reset(self::$instance[$instanceClassName]);
    
    return end(self::$instance[$instanceClassName]);
  } //function
  
  /**
   * Returns the last initialized object of this class.
   * If no object has been initialized yet, a new one will be returned.
   * 
   * @return \learnline\conf\classes\Config
   */
  public static function getInstance() {
    return self::getLastInstance();
  } //function

  /**
   * Wrapper for $this->getSettings() to be called as static method.
   * 
   * @param  mixed $setting
   * @return mixed
   * @throws \BadMethodCallException
   */
  public static function get($setting = NULL) {
    if (isset($setting)) {
      return self::getInstance()->getSetting($setting);
    } //if
    else {
      return self::getInstance()->getSettings();
    } //else
  } //function
  
  /**
   * Returns the value of given setting.
   * 
   * @param  mixed $setting
   * @return mixed
   * @throws \BadMethodCallException
   */
  public function getSetting($setting) {
    if (!array_key_exists($setting, $this->settings)) {
      throw new \BadMethodCallException(
          t("Cannot retrieve unknown setting '@setting'.", array(
              '@setting' => $setting)));
    } //if

    return $this->settings[$setting];
  } //function
  
  /**
   * Returns all current settings.
   * 
   * @return array
   */
  public function getSettings() {
    return $this->settings;
  } //function
  
  /**
   * Saves the setting named as parameter.
   * Simply omit the parameter to save all current values.
   * 
   * @param  mixed $setting
   * @return \learnline\conf\classes\Config
   * @throws \BadMethodCallException
   */
  public function save($setting = NULL) {
    if (isset($setting)) {
      if (!array_key_exists($setting, $this->settings)) {
        throw new \BadMethodCallException(
            t("Cannot save unknown setting '@setting'.", array(
                '@setting' => $setting)));
      } //if
      variable_set($setting, $this->settings[$setting]);
    } //if
    else {
      foreach ($this->settings as $setting => $value) {
        variable_set($setting, $value);
      } //foreach
    } //else
    
    return $this;
  } //function
  
  /**
   * Returns the number of setting items.
   * 
   * @return int
   */
  public function count() {
    return count($this->settings);
  } //function
  
  /**
   * Returns an ArrayIterator object to iterate settings.
   * 
   * @return \ArrayIterator
   */
  public function getIterator() {
    return new \ArrayIterator($this->settings);
  } //function
  
  /**
   * Gives information about whether a setting exists or not.
   * 
   * @param  mixed $offset Should be a valid array offset
   * @return bool
   */
  public function offsetExists($offset) {
    return array_key_exists($offset, $this->settings);
  } //function
  
  /**
   * Returns the appropriate setting's value.
   * 
   * @param  mixed $offset
   * @return mixed
   */
  public function offsetGet($offset) {
    return $this->settings[$offset];
  } //function
  
  /**
   * Override a setting.
   * The setting will not automatically persist. You will have to save it to the
   * database by calling $this->save().
   * 
   * @param mixed $offset
   * @param mixed $value
   */
  public function offsetSet($offset, $value) {
    $this->settings[$offset] = $value;
  } //function
  
  /**
   * Unsets a setting.
   * This operation will not persist!
   * 
   * @param mixed $offset
   */
  public function offsetUnset($offset) {
    unset($this->settings[$offset]);
  } //function
} //class
