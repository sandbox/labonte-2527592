<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use learnline\conf\classes\Config;

/**
 * Callback function for an overview of all learnline specific administration
 * pages.
 *
 * @return array Renderable content.
 */
function learnline_conf_menu_block_page() {
  $item = menu_get_item('admin/settings/learnline');

  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  } //if
  else {
    $output = t('It seems that you do not have sufficient rights to access these pages.');
  } //else

  return $output;
} //function

/**
 * Administration settings' form.
 *
 * @return array Renderable content.
 */
function learnline_conf_admin_settings() {
  global $base_url;
  $defaults = Config::get();
  $settings = array();
  
  $settings[Config::SETTING__WYSIWYG_HTML_FILTER_DISABLE] = array(
    '#type' => 'checkbox',
    '#title' => t('Omit filtering of handwritten HTML'),
    '#description' => t('Attention: This will turn off the WYSIWYG editor\'s content filter and therefore enable authors and editors to write potentially malicious code.'),
    '#default_value' => $defaults[Config::SETTING__WYSIWYG_HTML_FILTER_DISABLE],
  );

  $settings[Config::SETTING__OVERVIEW_TEASER_ENABLE] = array(
    '#type' => 'checkbox',
    '#title' => t("Add view mode 'overview_teaser'"),
    '#description' => t('This is necessary for correct display of overview article teasers.'),
    '#default_value' => $defaults[Config::SETTING__OVERVIEW_TEASER_ENABLE],
  );

  $settings[Config::SETTING__IMAGE_CAPTION_ENABLE] = array(
    '#type' => 'checkbox',
    '#title' => t('Build image captions'),
    '#description' => t("Wraps img tags with class 'show-caption' into a figure tag and uses the the img tag's title attribute as content for the corresponding caption tag."),
    '#default_value' => $defaults[Config::SETTING__IMAGE_CAPTION_ENABLE],
  );

  $settings[Config::SETTING__IMAGE_CAPTION_SELECTOR] = array(
    '#type' => 'textfield',
    '#title' => t('CSS-Selector for image captions'),
    '#description' => t('Defaults to <code>img.show-caption</code> to convert all <code>&lt;img&gt;</code>-Tags with CSS-Class <code>show-caption</code>.'),
    '#default_value' => $defaults[Config::SETTING__IMAGE_CAPTION_SELECTOR],
  );
  
  $settings[Config::SETTING__FIX_FIGCAPTION_WIDTH] = array(
    '#type' => 'checkbox',
    '#title' => t('Fix <code>&lt;figure&gt;</code>-tags'),
    '#description' => t("This fix removes floating of <code>&lt;img&gt;</code>-tags and adapts the width value of <code>&lt;figcaption&gt;</code>-tags to the one of the appropriate <code>&lt;img&gt;</code>-tag."),
    '#default_value' => $defaults[Config::SETTING__FIX_FIGCAPTION_WIDTH],
  );

  $settings[Config::SETTING__IMAGE_LINKS_ENABLE] = array(
    '#type' => 'checkbox',
    '#title' => t('Link images to page overlay'),
    '#description' => t("Transforms simple hyperlinks to image files into Javascript trigger, which will cause the images to load inside our page overlay."),
    '#default_value' => $defaults[Config::SETTING__IMAGE_LINKS_ENABLE],
  );

  $settings[Config::SETTING__IMAGE_LINKS_SELECTOR] = array(
    '#type' => 'textfield',
    '#title' => t('CSS-Selector for image links'),
    '#description' => t('Defaults to <code>@default</code> to convert all <code>&lt;img&gt;</code>-Tags with <em>rel</em> attribute matching "overlay", "lightbox" or "shadowbox".',
        array('@default' => $defaults[Config::SETTING__IMAGE_LINKS_SELECTOR])),
    '#default_value' => $defaults[Config::SETTING__IMAGE_LINKS_SELECTOR],
  );
  
  $settings[Config::SETTING__PERSIST] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep configuration persistent'),
    '#description' => t('Keep configuration in database even if the module is disabled and uninstalled.'),
    '#default_value' => $defaults[Config::SETTING__PERSIST],
  );
  
  $settings[Config::SETTING__ACTIVATE_ALERTIFY] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Alertify JS for notifications'),
    '#description' => l('More information about Alertify JS', 'http://fabien-d.github.io/alertify.js/'),
    '#default_value' => $defaults[Config::SETTING__ACTIVATE_ALERTIFY],
  );
  
  if (!is_dir(libraries_get_path('alertify.js'))) {
    $settings[Config::SETTING__ACTIVATE_ALERTIFY]['#disabled'] = TRUE;
    $settings[Config::SETTING__ACTIVATE_ALERTIFY]['#description'] = 
        t('To activate this setting place the alertify tool (>= v0.3) into <code>sites/all/libraries/alertify.js</code>.') .
        '<br/>' . $settings[Config::SETTING__ACTIVATE_ALERTIFY]['#description'];
  }
  
  $settings[Config::SETTING__ALLOWED_FILE_EXTENSIONS] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions'),
    '#description' => t("Specifies which files will be added when using the <em>Fix file management</em> button. Just enter a list of comma-seperated plain file extension like '<code>pdf,png,jpg</code>' and so on..."),
    '#default_value' => $defaults[Config::SETTING__ALLOWED_FILE_EXTENSIONS],
  );

  return system_settings_form($settings);
} //function
