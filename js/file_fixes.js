// $Id:$

/**
 * @file
 * 
 */

(function ($) {
  
$(document).ready(function () {
  var button = $('#learnline_conf_file_fixes_button');
  var msgBox = $('#learnline_conf_file_fixes_result');

  button.off()
      .after($('<div></div>').css('clear', 'both'))
      .css({
        float: 'left',
        clear: 'both',
        margin: 0,
        padding: '2px 5px',
        backgroundColor: '#E8E8D8',
        border: '2px outset'
      })
      .mouseenter(function () { $(this).css('borderStyle', 'inset'); })    
      .mouseleave(function () { $(this).css('borderStyle', 'outset'); })
      .on('click', function () {
        $.ajax('/admin/run_file_fixes', {
          beforeSend: function () {
            msgBox.empty();
          },
          success: function (data) {
            if (!data.added || !data.removed) {
              return;
            }

            $('<ul></ul>').appendTo(msgBox)
              .append($('<li>+ <em>' + data.added.length + '</em> files.</li>'))
              .append($('<li>- <em>' + data.removed.length + '</em> files.</li>'));
          },
          error: function (jqXHR, statusText, error) {
            msgBox.append(statusText);
          }
        });
        return false;
      });
});

})(jQuery);
