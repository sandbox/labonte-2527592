// $Id:$

/**
 * @file
 * Provides theme and layout relevant Javascript functions.
 */

if (window.learnline !== undefined) (function ($) {

  Drupal.behaviors.learnline_conf = {
    attach: function (context, settings) {
      if (settings.learnline === undefined) {
        return;
      }

      if (settings.learnline.conf.imgcaption.on) {
        LearnlineConf.buildFigures();
      }

      if (settings.learnline.conf.imglinks.on) {
        LearnlineConf.buildImageLinks();
      }

      if ($(settings.learnline.conf.fixes).length > 0) {
        for (var fix in settings.learnline.conf.fixes) {
          settings.learnline.conf.fixes[fix] && LearnlineConf.fixes[fix]();
        }
      }
    } //function
  };

  var LearnlineConf = {
    /**
     * Create figcaption tags.
     *
     * Fetch img tags by configurable selector. Then for each:
     *  1) Create <figure>-tag.
     *  2) Wrap <img>-tag into the new <figure>-tag.
     *  3) Add <figcaption>-tag based on img tag's title attribute.
     *  4) Replace the original <img>-tag.
     */
    buildFigures: function () {
      var selector = Drupal.settings.learnline.conf.imgcaption.selector;
      $(selector).each(function () {
        if (!this.title || this.title.length < 1) {
          return;
        } //if

        var img = $(this);
        var figure = $('<figure></figure>')
            .attr({
              'style': img.attr('style'),
              'class': img.attr('class')
            })
            .css({
              'height': 'auto',
              'border': 'none'
            });

        var dimensions = {
          innerWidth: img.width(),
          outerWidth: img.outerWidth(),
          innerHeight: img.height(),
          outerHeight: img.outerHeight()
        };

        if (dimensions.innerWidth < dimensions.outerWidth) {
          var diff = dimensions.outerWidth - dimensions.innerWidth;
          learnline.DEBUG&2 && console.log('Correcting image width from ' + dimensions.innerWidth + ' to ' + (dimensions.innerWidth - diff) + ', to fit the figure.');
          img.css('width', (dimensions.innerWidth - diff) + 'px');
        }

        if (dimensions.innerHeight < dimensions.outerHeight) {
          learnline.DEBUG&2 && console.log('Correcting image height from ' + dimensions.innerHeight + ' to ' + (dimensions.innerHeight - diff) + ', to fit the figure.');
          var diff = dimensions.outerHeight - dimensions.innerHeight;
          img.css('height', (dimensions.innerHeight - diff) + 'px');
        }

        var figcaption = $('<figcaption></figcaption>')
            .css({
              'width': img.outerWidth() + 'px',
              'textAlign': 'left',
              'fontSize': '80%'
            })
            .html(img.attr('title'));

        figure.append(img.clone().css({
                'float': 'none',
                'margin': '0'
              })
            )
            .append(figcaption);

        if (typeof img.attr('style') === 'string') {
          var floating_direction = /float:\s*(left|right);/i;
          var floating = img.attr('style').match(floating_direction);
          if (floating !== null && floating.length > 1) {
            learnline.DEBUG&2 && console.log(
              'Add floating layout for auto-captioned images: ' + floating[1]
            );
            figure.addClass(floating[1]);
          }
        } //if

        img.replaceWith(figure);
      });
    },
    
    /**
     * Manipulate image links.
     *
     * Fetch img tags by configurable selector. Then for each:
     *  1) Create <figure>-tag.
     *  2) Wrap <img>-tag into the new <figure>-tag.
     *  3) Add <figcaption>-tag based on img tag's title attribute.
     *  4) Replace the original <img>-tag.
     */
    buildImageLinks: function () {
      var selector = Drupal.settings.learnline.conf.imglinks.selector;
      $(selector).each(function () {
        var link = $(this);
        new learnline.PageOverlay(link, link.attr('href'));
//        link.click(function () {
//          return false;
//        });
      });
    },
    
    fixes: {
      /**
       * Fix width of server-side created <figcaption> tags that are bigger than
       * the captioned image.
       */
      figcaption_width: function () {
        $('figure').each(function () {
          // Fix <figcaption> width:
          $(this).children('figcaption').width(
              $(this).children('img').css('float', 'none').outerWidth());
          learnline.DEBUG&2 && console.log('Fixed <figure>: ' + this);
        });
      }
    }
  };

})(jQuery);
